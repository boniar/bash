#!/bin/bash
echo -e "\e[30;43m<HOSTNAME INFORMATION>\e[0m"
hostnamectl
hostname -I

echo -e "\e[30;43m<SYSTEM UPTIME and AVERAGE LOAD>\e[0m"
uptime

echo -e "\e[30;43m<CURRENTLY LOGGED-IN USERS>\e[0m"
w

echo -e "\e[30;43m<FILE SYSTEM DISK SPACE USAGE>\e[0m"
df -ah

echo -e "\e[30;43m<FREE AND USED MEMORY>\e[0m"
free -h

echo -e "\e[30;43m<TEN MOST CONSUMING PROCESSES>\e[0m"
ps -eo %mem,%cpu,comm --sort=-%mem | head -n 11

echo -e "\e[30;43m<LIST OF ACTIVE/INACTIVE SERVICES>\e[0m"
service --status-all

echo -e "\e[30;43m<LIST OF OPEN PORTS>\e[0m"
netstat -tulpn