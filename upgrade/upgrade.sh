#!/bin/bash
#A script that makes it easier to update system packages.
Date=$(date +"%F")

#Function that updates & upgrade packages and leaves the current log file
function update()
{
    sudo apt-get update |& tee -a log$Date.log &> /dev/null
    sudo apt-get upgrade -y |& tee -a log$Date.log &> /dev/null
    echo "system up to date!"
}

update